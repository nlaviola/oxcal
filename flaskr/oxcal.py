from flask import (
    Blueprint, current_app, flash, g, jsonify, redirect, render_template, request, send_from_directory, url_for
)
import os
from werkzeug.exceptions import abort

from marshmallow.exceptions import ValidationError
from sqlalchemy import exc

from .models import db, Analysis, Part, Oxcal, AnalysisSchema, PartSchema, OxcalSchema

bp = Blueprint('oxcal', __name__, url_prefix='/oxcals')

#landing page with table of oxcals
@bp.route('/')
def index():
  #query for oxcals
  oxcals = Oxcal.query.with_entities(Oxcal.cal_num, Oxcal.run, Oxcal.device, Oxcal.wavelength, Oxcal.status).order_by(Oxcal.cal_num.desc()).all()

  #loop through result and build dictionary of oxcals with devices tested and statuses used
  oxcals_final = {}
  for cal in oxcals:
    if not cal.cal_num in oxcals_final:
      oxcals_final[cal.cal_num] = {'cal_num': cal.cal_num, 'run': cal.run, 'wavelength': cal.wavelength, 'devices': list()}

    oxcals_final[cal.cal_num]['devices'].append(cal.device)
    
  return render_template('oxcal/index.html', oxcals=oxcals_final)

@bp.route('view_oxcal/<int:cal_num>')
def view_oxcal(cal_num):
  #query for oxcal information
  oxcal_data = Oxcal.query.filter(Oxcal.cal_num==cal_num).order_by(Oxcal.unix_timestamp.asc())

  page_data = {'oxcal_data': oxcal_data, 'cal_num': cal_num}

  return render_template('oxcal/view_oxcal.html', page_data=page_data)

@bp.route('get_oxcal_data_ajax', methods=['POST'])
def get_oxcal_data_ajax():
  if request.method == 'POST':
    cal_num = request.form['cal_num']

    #query for oxcal info order by device & corresponding analysis data
    oxcal_analysis_data = db.session().query(Oxcal, Analysis).filter(Oxcal.cal_num==cal_num).outerjoin(Analysis, Oxcal.id == Analysis.oxcal_id).order_by(Oxcal.device.asc()).all()

    #prep schemas
    oxcal_schema = OxcalSchema()
    analysis_schema = AnalysisSchema()

    #build final data object
    oxcal_final = {}
    for row in oxcal_analysis_data:
      oxcal_row = oxcal_schema.dump(row[0])
      if row[1]:
        analysis_row = analysis_schema.dump(row[1])

      if not oxcal_row['device'] in oxcal_final:
        oxcal_final[oxcal_row['device']]= {'rows': list()}
      if row[1]:
        oxcal_row['analysis'] = analysis_row
      oxcal_final[oxcal_row['device']]['rows'].append(oxcal_row)

    #return JSON
    return jsonify(oxcal_final)

@bp.route('get_oxcal_data_devices_ajax', methods=['POST'])
def get_oxcal_data_devices_ajax():
  if request.method == 'POST':
    cal_num = request.form['cal_num']

    #query for oxcal info order by device
    oxcal_devices = Oxcal.query.filter(Oxcal.cal_num==cal_num).with_entities(Oxcal.device).distinct().order_by(Oxcal.device.asc())

    oxcal_schema = OxcalSchema(many=True)
    #transform to list of rows as dicts
    oxcal_devices = oxcal_schema.dump(oxcal_devices)
    #transform to list of devices
    oxcal_devices_final = list()
    for row in oxcal_devices:
      oxcal_devices_final.append(row["device"])

    #return JSON
    return jsonify(oxcal_devices_final)


@bp.route('send_file/<int:cal_num>/<device>/<hl>/<status>/<int:unix_timestamp>')
def send_file(cal_num,device,hl,status,unix_timestamp):
  #build path to image
  file_path = f"uploads/oxcal_files/oxcal{cal_num}/{device}/oxcal{cal_num}_{device}_{hl}_{status}_{unix_timestamp}.png"

  return send_from_directory(os.path.join(current_app.instance_path), file_path)

@bp.route('update_oxcal_data_ajax', methods=['PUT'])
def update_oxcal_data_ajax():
  if request.method == 'PUT':
    #get request data
    row_id = request.form['row_id']
    run = request.form['run']
    comments = request.form['comments']

    #query for oxcal row
    oxcal_row = Oxcal.query.get(row_id)
    #make changes
    oxcal_row.run = run
    oxcal_row.comments = comments
    #commit changes
    db.session.commit()

    return jsonify({"id": oxcal_row.id, "code": 204})

@bp.route('update_oxcal_flag_ajax', methods=['PUT'])
def update_oxcal_flag_ajax():
  if request.method == 'PUT':
    #get request data
    id = request.form['id']
    flag = request.form['flag']

    #query for oxcal row
    oxcal_row = Oxcal.query.get(id)
    #make changes
    oxcal_row.flag = flag
    #commit changes
    db.session.commit()

    return jsonify({"id": oxcal_row.id, "code": 204})


