"""empty message

Revision ID: ce9ee6063587
Revises: c7278143ee06
Create Date: 2021-09-28 12:45:15.160111

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ce9ee6063587'
down_revision = 'c7278143ee06'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('analysis', schema=None) as batch_op:
        batch_op.add_column(sa.Column('cal_num', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('run', sa.Integer(), nullable=True))
        batch_op.add_column(sa.Column('device', sa.String(length=32), nullable=True))
        batch_op.add_column(sa.Column('status', sa.String(length=32), nullable=True))
        batch_op.add_column(sa.Column('comments', sa.Text(), nullable=True))

    with op.batch_alter_table('oxcals', schema=None) as batch_op:
        batch_op.add_column(sa.Column('wafers', sa.Text(), nullable=True))
        batch_op.add_column(sa.Column('oven_temp', sa.Float(), nullable=True))
        batch_op.add_column(sa.Column('oven_time', sa.Float(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('oxcals', schema=None) as batch_op:
        batch_op.drop_column('oven_time')
        batch_op.drop_column('oven_temp')
        batch_op.drop_column('wafers')

    with op.batch_alter_table('analysis', schema=None) as batch_op:
        batch_op.drop_column('comments')
        batch_op.drop_column('status')
        batch_op.drop_column('device')
        batch_op.drop_column('run')
        batch_op.drop_column('cal_num')

    # ### end Alembic commands ###
