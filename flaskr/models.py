"""Data models."""
from . import db, ma
from marshmallow import  Schema, fields, validates_schema, ValidationError
from pathvalidate import validate_filename, ValidationError as patherr

class Analysis(db.Model):
  """Data model for analysis"""

  __tablename__ = 'analysis'
  id = db.Column(db.Integer, primary_key=True)
  oxcal_id = db.Column(db.Integer, db.ForeignKey('oxcals.id'))
  cal_num = db.Column(db.Integer)
  run = db.Column(db.String(32))
  device = db.Column(db.String(32))
  status = db.Column(db.String(32))
  percent_diff = db.Column(db.Float)
  nir_long_axis = db.Column(db.Float)
  nir_short_axis = db.Column(db.Float)
  comments = db.Column(db.Text)
  unix_timestamp = db.Column(db.Integer)

class Part(db.Model):
  """Data model for parts"""

  __tablename__ = 'parts'
  id = db.Column(db.Integer, primary_key=True)
  cal_num = db.Column(db.Integer)
  device = db.Column(db.String(32))

  def __repr__(self):
    return '<Part {}>'.format(self.id)

class Oxcal(db.Model):
  """Data model for oxcals"""

  __tablename__ = 'oxcals'
  id = db.Column(db.Integer, primary_key=True)
  part_id = db.Column(db.Integer, db.ForeignKey('parts.id'))
  cal_num = db.Column(db.Integer)
  run = db.Column(db.String(32))
  device = db.Column(db.String(32))
  maker = db.Column(db.String(32))
  wafers = db.Column(db.Text)
  wavelength = db.Column(db.Integer)
  pump_wl = db.Column(db.Float)
  pump_current = db.Column(db.Float)
  wavelength = db.Column(db.Float)
  status = db.Column(db.String(32))
  oven_temp = db.Column(db.Float)
  oven_time = db.Column(db.Float)
  lcr_high = db.Column(db.Float)
  lcr_low = db.Column(db.Float)
  comments = db.Column(db.Text)
  user = db.Column(db.String(32))
  flag = db.Column(db.Integer)
  unix_timestamp = db.Column(db.Integer)

  def __repr__(self):
    return '<OxCal {}>'.format(self.cal_num)

class AnalysisSchema(ma.SQLAlchemyAutoSchema):

  class Meta:
    model = Analysis

class PartSchema(ma.SQLAlchemyAutoSchema):
  class Meta:
    model = Part

class OxcalSchema(ma.SQLAlchemyAutoSchema):
  #set required fields
  cal_num = fields.Int(required=True)
  device = fields.Str(required=True)
  status = fields.Str(required=True)

  #custom validation of status code for purpose of filename
  @validates_schema
  def validate_status(self, data, **kwargs):
    try:
      validate_filename(data['status'])
    except patherr as e:
      error = f"{e}\n"
      raise ValidationError(error)
   
  class Meta:
    model = Oxcal

