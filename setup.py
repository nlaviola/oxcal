from setuptools import find_packages, setup

setup(
    name='flaskr',
    version='1.0.4',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
        'Flask-SQLAlchemy>=2.5.1',
        'flask-marshmallow>=0.14.0',
        'marshmallow-sqlalchemy>=0.26.1',
        'Flask-Migrate>=3.1.0',
        'pandas>=1.3.2 ',
        'pathvalidate>=2.4.1'
    ],
)