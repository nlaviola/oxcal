import io, json, os, zipfile, time
from os.path import basename
import pandas as pd
from marshmallow.exceptions import ValidationError
from sqlalchemy import exc
from flask import (
  abort, Blueprint, flash, g, jsonify, redirect, render_template, request,  send_file, session, url_for
)
from flask import current_app
from werkzeug.utils import secure_filename

from .models import db, Analysis, Part, Oxcal, AnalysisSchema, PartSchema, OxcalSchema

bp = Blueprint('api', __name__, url_prefix='/api')

#adds a new analysis
@bp.route('/analysis', methods=['POST'])
def add_analysis():
  if request.method == 'POST':
    # get the request data
    ox = request.get_json().get("oxcal")
    analysis = request.get_json().get("analysis")
    # validate incoming ox data
    oxcal_schema = OxcalSchema()
    analysis_schema = AnalysisSchema()
    try:
      oxcal_data = oxcal_schema.load(ox)
      analysis_data = analysis_schema.load(analysis)
    except ValidationError as e:
      return {"errors": e.messages}, 422
    
    # query for the oxcal requested
    try:
      requested_oxcal = Oxcal.query.filter(Oxcal.cal_num==oxcal_data['cal_num'], Oxcal.device==oxcal_data['device'], Oxcal.status==oxcal_data['status']).one_or_none()
    except exc.SQLAlchemyError as e:
      return {"error": "This Oxcal row does not exist!"}, 400

    #fill the analysis object
    analysis_data['oxcal_id'] = requested_oxcal.id
    analysis_data['cal_num'] = requested_oxcal.cal_num
    analysis_data['run'] = requested_oxcal.run
    analysis_data['device'] = requested_oxcal.device
    analysis_data['status'] = requested_oxcal.status
    analysis_data['unix_timestamp'] = int(time.time())

    # commit the analysis
    try:
      analysis_final = Analysis(**analysis_data)
      db.session.add(analysis_final)
      db.session.commit()
    except exc.SQLAlchemyError as e:
      db.session.rollback()
      return {"error": "Problem inserting Oxcal Data, please contact the admin"}, 400

    return jsonify({"id": analysis_final.id}), 201


# returns all oxcals in the db 
@bp.route('/oxcals')
def get_oxcals():
  #query for all oxcals
  oxcals = Oxcal.query.order_by(Oxcal.unix_timestamp.desc()).all()
  oxcal_schema = OxcalSchema(many=True)
  output = jsonify(oxcal_schema.dump(oxcals))

  return output, 200

# adds a new oxcal
# adds a new part if the part does not exists in the db
@bp.route('/oxcals', methods=['POST'])
def add_oxcal():
  #add a new oxcal
  if request.method == 'POST':

    oxcal_schema = OxcalSchema()

    file_high = request.files['image_high']
    file_low = request.files['image_low']
    data = json.load(request.files['data']) # oxcal_data variable is actually a dict

    #validate files
    file_high_ext = os.path.splitext(file_high.filename)[1]
    file_low_ext = os.path.splitext(file_low.filename)[1]

    #validate .png files
    if file_high_ext not in current_app.config['UPLOAD_EXTENSIONS'] or file_low_ext not in current_app.config['UPLOAD_EXTENSIONS']:
      return {'error': 'File format must be .png'}, 400
    
    #validate incoming data
    try:
      oxcal_data = oxcal_schema.load(data)
    except ValidationError as e:
      return {"errors": e.messages}, 422

    # create new part record if it does not exist
    part = Part.query.filter(Part.cal_num==oxcal_data['cal_num']).filter(Part.device==oxcal_data['device']).first()
    if part is None:
      new_part = {
        'cal_num':oxcal_data['cal_num'],
        'device':oxcal_data['device']
      }
      try:
        part = Part(**new_part)
        db.session.add(part)
        db.session.commit()
      except exc.SQLAlchemyError as e:
        db.session.rollback()
        return {"error": "Problem creating new part, please contact the admin"}, 400
      
    # add part number to OxCal data
    oxcal_data['part_id'] = part.id

    # insert the oxcal data
    try:
      oxcal = Oxcal(**oxcal_data)
      db.session.add(oxcal)
      db.session.commit()
    except exc.SQLAlchemyError as e:
      db.session.rollback()
      return {"error": "Problem inserting Oxcal Data, please contact the admin"}, 400

    # #create the device folder for the oxcal files
    oxcal_path = os.path.join(current_app.instance_path, f"uploads/oxcal_files/oxcal{oxcal.cal_num}/{oxcal.device}")
    os.makedirs(oxcal_path, exist_ok=True)
    #write data to csv file
    data_list = list()
    data_list.append(oxcal_data)
    pd.DataFrame.from_dict(data_list).to_csv(oxcal_path+ f"/oxcal{oxcal.cal_num}_{oxcal.device}_{oxcal.status}_data.csv", index=False)
    # save high
    filename_high = f"oxcal{oxcal.cal_num}_{oxcal.device}_high_{oxcal.status}_{oxcal.unix_timestamp}.png"
    file_high.save(os.path.join(current_app.instance_path, oxcal_path, secure_filename(filename_high)))
    #save low
    filename_low = f"oxcal{oxcal.cal_num}_{oxcal.device}_low_{oxcal.status}_{oxcal.unix_timestamp}.png"
    file_low.save(os.path.join(current_app.instance_path, oxcal_path, secure_filename(filename_low)))
    
    return jsonify({"id": oxcal.id}), 201

# @bp.route('/complete_oxcal/<int:cal_num>')
# def get_complete_oxcal(cal_num, methods=['GET']):
#   if cal_num:

#     # file directory
#     oxcal_folder_path = os.path.join(current_app.instance_path, f"uploads/oxcal_files/oxcal{cal_num}/")
#     zip_folder_name = f"oxcal{cal_num}_files.zip"
#     zipfolder = zipfile.ZipFile(zip_folder_name,'w', compression = zipfile.ZIP_STORED)

#     # zip all the files which are inside in the folder
#     for root,dirs, files in os.walk(oxcal_folder_path):
#       for file in files:
#         zipfolder.write(oxcal_folder_path+file, basename(oxcal_folder_path+file)) # 2nd parameter is arcname
#     zipfolder.close()

#     #write the file to memory to send to user
#     return_data = io.BytesIO()
#     with open(zip_folder_name, 'rb') as fo:
#       return_data.write(fo.read())
#     # (after writing, cursor will be at last byte, so move it to start)
#     return_data.seek(0)
    
#     # delete the actual file
#     os.remove(zip_folder_name)

#     return send_file(return_data,
#             mimetype = 'zip',
#             attachment_filename= zip_folder_name,
#             as_attachment = True), 200

@bp.route('/single_oxcal_row/<int:cal_num>/<device>/<status>')
def get_single_oxcal_row(cal_num, device, status, methods=['GET']):
  if cal_num and device and status:

    # query for oxcal
    try:
      requested_oxcal = Oxcal.query.filter(Oxcal.cal_num==cal_num, Oxcal.device==device, Oxcal.status==status).one_or_none()
    except exc.SQLAlchemyError as e:
      return {"error": "This Oxcal row does not exist!"}, 400

    # file directory
    oxcal_folder_path = os.path.join(current_app.instance_path, f"uploads/oxcal_files/oxcal{requested_oxcal.cal_num}/{requested_oxcal.device}/")
    zip_folder_name = f"oxcal{requested_oxcal.cal_num}_{requested_oxcal.device}_{requested_oxcal.status}_files.zip"
    zipfolder = zipfile.ZipFile(zip_folder_name,'w', compression = zipfile.ZIP_STORED)

    # build filenames
    high_img = f"oxcal{requested_oxcal.cal_num}_{requested_oxcal.device}_high_{requested_oxcal.status}_{requested_oxcal.unix_timestamp}.png"
    low_img = f"oxcal{requested_oxcal.cal_num}_{requested_oxcal.device}_low_{requested_oxcal.status}_{requested_oxcal.unix_timestamp}.png"
    data_file = f"oxcal{requested_oxcal.cal_num}_{requested_oxcal.device}_{requested_oxcal.status}_data.csv"

    # zip all the files which are inside in the folder
    for root,dirs, files in os.walk(oxcal_folder_path):
      for file in files:
        if file == high_img or file == low_img or file == data_file:
          zipfolder.write(oxcal_folder_path+file, basename(oxcal_folder_path+file)) # 2nd parameter is arcname
    zipfolder.close()

    #write the file to memory to send to user
    return_data = io.BytesIO()
    with open(zip_folder_name, 'rb') as fo:
      return_data.write(fo.read())
    # (after writing, cursor will be at last byte, so move it to start)
    return_data.seek(0)
    
    # delete the actual file
    os.remove(zip_folder_name)

    return send_file(return_data,
            mimetype = 'zip',
            attachment_filename= zip_folder_name,
            as_attachment = True), 200

    
    




