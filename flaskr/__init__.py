import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate

db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
 
def create_app(test_config=None):
  # create and configure the app
  app = Flask(__name__, instance_relative_config=True)
  app.config.from_mapping(
    SECRET_KEY='dev',
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(app.instance_path, 'oxcal.db'),
    SQLALCHEMY_ECHO = False,
    SQLALCHEMY_TRACK_MODIFICATIONS = False,
    UPLOAD_EXTENSIONS = ['.png']
  )

  if test_config is None:
    # load the instance config, if it exists, when not testing
    app.config.from_pyfile('config.py', silent=True)
  else:
    # load the test config if passed in
    app.config.from_mapping(test_config)

  # ensure the instance folder exists
  try:
      os.makedirs(app.instance_path)
  except OSError:
      pass

  db.init_app(app)
  ma.init_app(app)
  migrate.init_app(app, db, render_as_batch=True)

  with app.app_context():
    from .models import Part, Oxcal
    db.create_all()

    #create path for photo uploads
    os.makedirs(os.path.join(app.instance_path, 'uploads/oxcal_files'), exist_ok=True)

    #import blueprint
    from . import api, oxcal
    app.register_blueprint(api.bp)
    app.register_blueprint(oxcal.bp)

    return app


  